from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Add_Project_Form
from .models import Project


# Create your views here.
def index(request):
    return render(request,'website.html')
def index2(request):
    return render(request,'website2.html')
def index3(request):
    return render(request,'form.html')



response = {}


def addproject(request):
	response['title'] = 'Add Project'
	response['addproject_form'] = Add_Project_Form

	html = 'form.html'

	return render(request, html, response)

def saveproject(request):
	form = Add_Project_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['position'] = request.POST['position']
		response['title'] = request.POST['title']
		response['description'] = request.POST['description']
		project = Project(position=response['position'], title=response['title'],desc=response['description'])
		project.save()
		return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('/')
