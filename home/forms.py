from django import forms

class Add_Project_Form(forms.Form):
    error_messages = {
        'required': 'Please type',
    }
    nama_kegiatan_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Your Activity'
    }
    kategori_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Kategori'
    }
    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'Description of the project'
    }

    position = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=nama_kegiatan_attrs))
    title = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=kategori_attrs))
    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))